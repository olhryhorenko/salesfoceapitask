
import java.io.IOException;
import java.util.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.json.JSONException;
public class ObjInsert extends HttpServlet{
    private static final long serialVersionUID = 1L;
    static final String PASS = "XXXXXXXXXXX";
    static final String SecurityToken = "6Cel800D5J00xxxxxxxx885J000000VIp26l2bce70GvgPnxpvWONrvsXbN39pQOvhRWzOFpSyUYh22WLoIgFn1XgQ20ajvVOrTICLDKp4";
    static final String USERNAME = "XXXXXXXXXXXXX";
    static final String PASSWORD = PASS + SecurityToken;
    static final String LOGINURL = "https://login.salesforce.com";
    String loginURL = LOGINURL + GRANTSERVICE + "&client_id=" + CLIENTID + "&client_secret=" + CLIENTSECRET + "&username=" + USERNAME + "&password=" + PASSWORD;
    static final String GRANTSERVICE = "/services/oauth2/token?grant_type=password";
    static final String CLIENTID = "3MVG95G8WxiwV5xxxxxxxxxxxxxxxxxxx7OdBqm.5lTmoze3AYLz0Ciuf0_9DXgSmJ6fBqdk8H2Pp5u";
    static final String CLIENTSECRET = "174C2972B8467xxxxxxxxxxx3BB812BFD96DFA96E61E39C4E01DD8A82242FEDA7";
    ArrayList<String> accounts = new ArrayList<String>();

    public void doPost() throws ServletException, IOException {

        HttpClient httpclient = HttpClientBuilder.create().build();
        String loginURL = LOGINURL + GRANTSERVICE + "&amp;client_id=" + CLIENTID + "&amp;client_secret=" + CLIENTSECRET
                + "&amp;username=" + USERNAME + "&amp;password=" + PASSWORD;
        HttpPost httpPost = new HttpPost(loginURL);
        HttpResponse resp = null;
        try {
            resp = httpclient.execute(httpPost);
        } catch (ClientProtocolException cpException) {
            cpException.printStackTrace();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        final int statusCode = resp.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            System.out.println("Error authenticating to Force.com: " + statusCode);
            return;
        }
        String getResult = null;
        try {
            getResult = EntityUtils.toString(resp.getEntity());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
        JSONObject jsonObject = null;
        String loginAccessToken = null;
        String loginInstanceUrl = null;
        try {
            jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
            loginAccessToken = jsonObject.getString("access_token");
            loginInstanceUrl = jsonObject.getString("instance_url");
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
        System.out.println(resp.getStatusLine());
        System.out.println("Successful login");
        System.out.println("  instance URL: " + loginInstanceUrl);
        System.out.println("  access token/session ID: " + loginAccessToken);
        httpPost.releaseConnection();
    }
 private  void doInsert() throws ServletException, IOException {
     accounts.add("Acc name");
     doPost();
 }
    public static void main(String[] args) throws ServletException, IOException {
        ObjInsert objInsert = new ObjInsert();
        objInsert.doInsert();
    }
}